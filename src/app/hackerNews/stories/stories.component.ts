import { Component, OnInit, Input } from '@angular/core';
import { Story } from '../model/story';
import { HackerService } from '../services/hacker.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.css']
})
export class StoriesComponent implements OnInit {
  @Input() storiesLength: number;
  story: Story;
  constructor(private service: HackerService) { }

  ngOnInit(): void {
    this.service.getStory(+this.storiesLength).subscribe(data=>{
      this.story = data;
    })
  }

}
