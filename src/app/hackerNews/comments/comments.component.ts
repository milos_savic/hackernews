import { Component, OnInit, Input } from '@angular/core';
import { HackerService } from '../services/hacker.service';
import { Comment } from '../model/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input() commentId: number[];
  comment: Comment;
  showComment: boolean = true;
  commentKids: number[];
  
  constructor(private service: HackerService) { }

  ngOnInit(): void {
    this.service.getComment(+this.commentId).subscribe(data=>{
      this.comment = data;
      this.commentKids = data.kids;
    })
  }

  toggleComment(){
    this.showComment = !this.showComment;
  }

}
