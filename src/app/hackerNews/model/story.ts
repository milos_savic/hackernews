export class Story {
    _id: number;
    by: string;
    descendants: number;
    kids: number[];
    score: number;
    time: number;
    title: string;
    type: string;
    url: string;

    constructor(obj?: any){
        this._id = obj && obj.id || null;
        this.by = obj && obj.by || "";
        this.descendants = obj && obj.descendants || 0;
        this.kids = obj && obj.kids || null;
        this.score = obj && obj.score || null;
        this.time = obj && obj.time || null;
        this.title = obj && obj.title || "";
        this.type = obj && obj.type || "";
        this.url = obj && obj. url || "";
    }

}