import { NgIf } from '@angular/common';

export class Item {
    _id: number;
    type: string;
    by: string;
    time: number;
    text: string;
    parent: number;
    kids: number[];
    url: string;
    score: number;
    title: string;

    constructor(obj?: any){
        this._id = obj && obj.id || null;
        this.type = obj && obj.type || "";
        this.by = obj && obj.by || "";
        this.time = obj && obj.time || null;
        this.text = obj  && obj.text || "";
        this.parent = obj && obj.parent || null;
        this.kids = obj && obj.kids || null;
        this.url = obj && obj.url || "";
        this.score = obj && obj.score || null;
        this.title = obj && obj.title || "";
    }
}