import { Item } from './item';

export class ItemList {
    results: Item[];

    constructor(obj?: any){
        this.results = obj || [];
    }
}