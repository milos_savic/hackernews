export class Comment {
    _id: number;
    by: string;
    kids?: number[];
    parent: number;
    text: string;
    time: number;
    type: string;
    deleted?: boolean;
    dead?: boolean;

    constructor(obj?: any){
        this._id = obj && obj.id || null;
        this.by = obj && obj.by || "";
        this.kids = obj && obj.kids || null;
        this.parent = obj && obj.parent || 0;
        this.text = obj && obj.text || "";
        this.time = obj && obj.time || null;
        this.type = obj && obj.type || "";
        this.dead = obj && obj.deleted || null;
        this.deleted = obj && obj.deleted || null;
    }

}