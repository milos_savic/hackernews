import { Component, OnInit, Input } from '@angular/core';
import { HackerService } from '../services/hacker.service';
import { Comment } from '../model/comment';

@Component({
  selector: 'app-comment-kids',
  templateUrl: './comment-kids.component.html',
  styleUrls: ['./comment-kids.component.css']
})
export class CommentKidsComponent implements OnInit {
  @Input() commentKid;
  comment: Comment
  commentKids
  showComment: boolean = true;

  constructor( private service: HackerService ) { }

  ngOnInit(): void {
    this.service.getComment(+this.commentKid).subscribe(data=>{
      this.comment = data;
      this.commentKids = data.kids;
    })
  }

  toggleComment(){
    this.showComment = !this.showComment;
  }

}
