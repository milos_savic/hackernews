import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentKidsComponent } from './comment-kids.component';

describe('CommentKidsComponent', () => {
  let component: CommentKidsComponent;
  let fixture: ComponentFixture<CommentKidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentKidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentKidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
