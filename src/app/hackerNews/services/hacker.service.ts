import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ItemList } from '../model/itemList';
import { map } from 'rxjs/operators'
import { Story } from '../model/story';
import { Comment } from '../model/comment';

const topStoriesUrl = "https://hacker-news.firebaseio.com/v0/topstories.json";
const itemsUrl = "https://hacker-news.firebaseio.com/v0/item/"

@Injectable({
  providedIn: 'root'
})
export class HackerService {

  constructor(private http: HttpClient) { }

  //ids of top stories//
  getItemList(): Observable<ItemList> {
    return this.http.get(topStoriesUrl).pipe(map(data=>{
      return new ItemList(data);
    }))
  }

  getStory(id: number): Observable<Story> {
    return this.http.get(itemsUrl + id + ".json").pipe(map(data=>{
      return new Story(data);
    }))
  }

  getComment(id: number): Observable<Comment> {
    return this.http.get(itemsUrl + id + ".json").pipe(map(data=>{
      return new Comment(data);
    }))
  }
}
