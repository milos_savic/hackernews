import { Component, OnInit } from '@angular/core';
import { ItemList } from '../model/itemList';
import { HackerService } from '../services/hacker.service';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.css']
})
export class StoryListComponent implements OnInit {
  storyList: ItemList;
  storiesLength: any;
  showStories: number = 15;

  constructor(private service: HackerService) { }

  ngOnInit(): void {
    this.refreshList();
  }

  refreshList(){
    this.service.getItemList().subscribe(data=>{
      this.storyList = data;
      this.storiesLength = this.storyList?.results.slice(0, this.showStories)
    })
  }

  showMoreStories(){
    this.showStories += 10;
    this.refreshList();
  }

}
