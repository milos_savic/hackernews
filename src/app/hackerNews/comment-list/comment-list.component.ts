import { Component, OnInit } from '@angular/core';
import { HackerService } from '../services/hacker.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {
  commentIds: number[];
  commentsLength: any;
  showComments: number = 10;
  constructor(private service: HackerService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      this.refreshComments();
      }

  refreshComments(){
    this.activatedRoute.params.subscribe(data=>{
      let id = parseInt(data['id']);
      this.service.getStory(id).subscribe(data=>{
        this.commentIds = data.kids;
        this.commentsLength = this.commentIds.slice(0, this.showComments)
      })
    })
  }

  showMoreComments(){
    this.showComments += 10;
    this.refreshComments();
  }

}
