import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { StoryListComponent } from './hackerNews/story-list/story-list.component';
import { StoriesComponent } from './hackerNews/stories/stories.component';
import { CommentListComponent } from './hackerNews/comment-list/comment-list.component';
import { CommentsComponent } from './hackerNews/comments/comments.component';
import { CommentKidsComponent } from './hackerNews/comment-kids/comment-kids.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    StoryListComponent,
    StoriesComponent,
    CommentListComponent,
    CommentsComponent,
    CommentKidsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
