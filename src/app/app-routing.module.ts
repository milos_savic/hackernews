import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoryListComponent } from './hackerNews/story-list/story-list.component';
import { CommentListComponent } from './hackerNews/comment-list/comment-list.component';


const routes: Routes = [
  {path:"stories", component:StoryListComponent},
  {path:"stories/:id/comments", component:CommentListComponent},
  {path:"", redirectTo:"/stories", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
